# Data logs microservice  

## How to run
**Prerequisite:** `docker` and `docker-compose` installed

**Navigate to the project root and simply run:**  
`$ source setup.sh`

The project's UI will be open on your browser, press the Refresh button to get 10 latest data (order by as fail_rate desc).

**Explain:**  
The script will prepare some evironment vars which store in `.env.sample` file and can be editted on demand.  
Then, setup the app which include **3 components**:
- An service UI which visualize data records
- An API service serves POST (for insert data into DB) and GET (for get data from DB) requests as required
- A Mongo database service for data storage

After bringing up the app, the script call `client.py` which performs 20 POST requests to insert fake data into the database.

And finally, open the UI on your browser.

## About the test requirements
### Strategy to store data
The data is migrated from a data source and might change its schema in the future, so we should use a NoSQL database for easly adapte to schema changing, in this case, I choose MongoDB which is a common NoSQL and have very intuitive documentation.

Our microservice provides a way to get *latest, sorted* by `fail_rate` records, so along with data fields take from source database, I add a `created_time` field to make this requirements possible. Also, index on these field are created to make the query faster.

- `fail_rate`: descending index because we need to sort in desc order
- `created_time`: descending index, the later time, the larger timestamp

### System Design
[<img src="system_design.png">]

### Unit test
The tests case is written using FastAPI built-in test tool `fastapi.testclient.TestClient` and can be run using `pytest`.

**To run the test, first setup local enviroment:**
- Create virtual environment using [conda](https://docs.conda.io/en/latest/miniconda.html): `$ conda create -n venv python=3.8`
- Activate the created venv: `$ conda activate venv`
- Install requirement: `$ pip install -r requirements-dev.txt`
  
**Run the test:** `$ cd api; python -m pytest tests/test_server.py`
### Code quality
Code quality check uses `flake8`.

**Run code quality check:**  
Ensure you've install all requirements (read Unit test section for details).  

```
# syntax errors or undefined names
$ flake8 . --count --select=E9,F63,F7,F82 --show-source --statistics`
# Check function type hints and return type
$ flake8 . --count --select=ANN001,ANN201,ANN202,ANN203,ANN204,ANN205,ANN206 --show-source --statistics
```

### CVE check
CVE check uses [docker scan](https://docs.docker.com/engine/scan/). Note that it's a docker's plugin and need to be installed, please follow the link for detail guides.

Run CVE check on api image:
`$ docker scan migrate-data-server/api:local-1.0`
