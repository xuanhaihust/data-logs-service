import os
import random
import time
import requests


api_url = 'http://localhost:' + os.getenv("API_PORT")

for _ in range(20):
    record = {
        "factory_id": 23871,
        "org_id": 2345,
        "country": "VN",
        "execution_date": "2022-05-01",
        "fail_rate": round(random.random(), 2),
        "defect_rate": round(random.random(), 2)
    }
    response = requests.post(api_url + '/' + "api/record", json=record)

    time.sleep(0.01)  # distinct the created_time for get_latest feature
