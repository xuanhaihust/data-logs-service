from fastapi.testclient import TestClient

from src.server import app


client = TestClient(app)


def test_health_check():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Service is healthy"}


def test_insert_record():
    record = {
        "factory_id": 23871,
        "org_id": 2345,
        "country": "VN",
        "execution_date": "2022-05-01",
        "fail_rate": 0.5,
        "defect_rate": 0.2
    }
    response = client.post(
        "api/record",
        json=record,
    )

    assert response.status_code == 200

    res_record = response.json()
    assert res_record["factory_id"] == record["factory_id"]
    assert res_record["org_id"] == record["org_id"]
    assert res_record["country"] == record["country"]
    assert res_record["execution_date"].split("T")[0] == record["execution_date"]
    assert res_record["fail_rate"] == record["fail_rate"]
    assert "created_time" in res_record


def test_get_record():
    params = {
        "factory_id": 23871,
        "org_id": 2345
    }
    response = client.get("/api/record", params=params)

    assert response.status_code == 200
    assert isinstance(response.json(), list)

    first_res_record = response.json()[0]
    assert first_res_record["factory_id"] == params["factory_id"]
    assert first_res_record["org_id"] == params["org_id"]
