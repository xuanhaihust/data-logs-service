import os


DB_CON_STR = os.getenv("DB_CON_STR") or "mongodb://localhost:27017"
DB_NAME = os.getenv("DB_NAME") or "data"
MONGO_MAX_POOL_SIZE = os.getenv("MONGO_MAX_POOL_SIZE") or 500
DATA_COLLECTION = os.getenv("DATA_COLLECTION") or "factory_logs"
