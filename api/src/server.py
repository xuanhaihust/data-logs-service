from datetime import date, datetime
from typing import Union, Literal, Optional
from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

from src.database import MongoDatabase
from src.config import DATA_COLLECTION


class Record(BaseModel):
    factory_id: int
    org_id: int
    country: str
    execution_date: date
    fail_rate: float
    defect_rate: float
    created_time: Union[datetime, None] = None


app = FastAPI()


origins_regex = r"http://localhost:\d+"

app.add_middleware(
    CORSMiddleware,
    allow_origin_regex=origins_regex,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/", status_code=200)
async def health_check():
    return {"message": "Service is healthy"}


@app.post("/api/record", status_code=200)
async def insert_record(record: Record):
    """Insert a new record into database
    :param record: request body in Record data model.
    :return: the processed and validated record.
    """

    record.created_time = datetime.utcnow()

    # mongodb only supports datetime
    record.execution_date = datetime.combine(
        record.execution_date, datetime.min.time())

    data_col = MongoDatabase.get_collection('factory_logs')
    data_col.insert_one(record.dict())

    return record


@app.get("/api/record", status_code=200)
async def get_record(request: Request,
                     factory_id: Optional[int] = None,
                     org_id: Optional[int] = None,
                     sort_fail_rate: Literal['asc', 'desc'] = None,
                     get_latest: Optional[bool] = False,
                     limit: int = 10):
    """Get records which is inserted in the database base on some filters.
    :param request: the fastapi request itself, contain all things relate to the request.
    :param factory_id: only records with this value of factory_id
    :param org_id: only record with this value of org_id
    :param sort_fail_rate: sort the result by fail_rate field value
    :param get_latest: get the latest records based on created_time
    :param limit: return only `limit` records
    :return: json. List of records that satisfy the request params.
    """
    try:
        country = request.query_params['country']
    except KeyError:
        country = None

    try:
        execution_date = request.query_params['execution_date']
    except KeyError:
        execution_date = None

    match = {}
    if org_id:
        match["org_id"] = org_id
    if factory_id:
        match["factory_id"] = factory_id
    if country:
        match["country"] = country
    if execution_date:
        match["execution_date"] = execution_date

    projection = {"_id": 0}

    sort_condition = dict()
    if get_latest:
        sort_condition["created_time"] = -1

    result = MongoDatabase.mongo_find_query(DATA_COLLECTION,
                                            match, projection, sort_condition, limit)

    if sort_fail_rate == 'asc':
        result = sorted(result, key=lambda d: d['fail_rate'])
    elif sort_fail_rate == 'desc':
        result = sorted(result, key=lambda d: d['fail_rate'], reverse=True)

    return result


if __name__ == "__main__":
    import uvicorn
    uvicorn.run('server:app', host="0.0.0.0", port=5002, reload=True)
