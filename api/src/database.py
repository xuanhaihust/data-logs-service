from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

from src.config import DB_CON_STR, DB_NAME, MONGO_MAX_POOL_SIZE


class MongoDatabase:
    """
    MongoClient manager in singleton style.
    Each MongoClient object have a connection-pooling built-in,
    so we implement Singleton only for MongoClient.
    The use of database, collection depend on this built-in pooling.
    """
    __client = None

    @staticmethod
    def get_client(new_instant_bool=False):
        """Singleton-style client"""

        if new_instant_bool:
            return MongoClient(DB_CON_STR, maxPoolSize=MONGO_MAX_POOL_SIZE)

        if MongoDatabase.__client is None:
            assert DB_CON_STR is not None
            try:
                MongoDatabase.__client = MongoClient(
                    DB_CON_STR, maxPoolSize=MONGO_MAX_POOL_SIZE)
            except ConnectionFailure:
                raise ConnectionFailure(
                    "Can not create connection to mongodb, check your network or database instance")

        return MongoDatabase.__client

    @staticmethod
    def get_db(new_instant_bool=False):
        client = MongoDatabase.get_client(new_instant_bool)
        assert DB_NAME is not None

        return client.get_database(str(DB_NAME))

    @staticmethod
    def get_collection(name: str, new_instant_bool=False):
        assert name
        return MongoDatabase.get_db(new_instant_bool).get_collection(str(name))

    @staticmethod
    def create_index(collection: str, keys: dict, **kwargs) -> str:
        """
        :param collection: name of collection
        :param keys: dict of key: index_type pairs. If 1 pair -> single field index, if > 1 -> compound index
        Returns: index_name
        """
        col = MongoDatabase.get_collection(collection)

        return col.create_index([(field, index_type) for field, index_type in keys.items()], **kwargs)

    @staticmethod
    def drop_index(collection: str, index_name: str) -> None:
        """
        :param collection: name of collection
        :param index_name: name of the index to delete, taken by db.col.list_indexes()
        Return: None
        """
        col = MongoDatabase.get_collection(collection)
        col.drop_index(index_name)

    @staticmethod
    def list_indexes(collection: str) -> list:
        """
        List index names of the input collection.
        :param collection: collection to get index names
        :return: list of index names
        """
        col = MongoDatabase.get_collection(collection)
        indexes = col.index_information()

        return list(indexes.keys())

    @staticmethod
    def mongo_find_query(collection: str,
                         match: dict = None,
                         projection: dict = None,
                         sort_condition: dict = None,
                         limit: int = None
                         ) -> list:
        """Perform find query based on common params"""

        col = MongoDatabase.get_collection(collection)

        if not match:
            match = dict()
        if not projection:
            projection = dict()

        cursor = col.find(
            filter=match,
            projection=projection
        )

        if sort_condition:
            # pymongo.Cursor.sort() requires input must be key or list of conditions
            cursor.sort(list(sort_condition.items()))

        if limit:
            cursor.limit(limit)

        return list(cursor)
