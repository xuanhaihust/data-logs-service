set -o allexport
source .env.sample
set +o allexport
docker-compose --env-file .env.sample up -d --build --remove-orphans
sleep 3
python client.py
nohup xdg-open http://localhost:6969
# nohup xdg-open http://localhost:5002/docs